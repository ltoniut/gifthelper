import Vue from 'vue';
import App from './App.vue';
import Gifts from './Gifts.vue';

Vue.component('app-gifts', Gifts);

new Vue({
  el: '#app',
  render: h => h(App)
});
